import 'package:flutter/material.dart';
import 'package:gdp_v2/Utils/Loading.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'package:auto_size_text/auto_size_text.dart';

class Records extends StatefulWidget {
  @override
  RecordsState createState() {
    return new RecordsState();
  }
}

class RecordsState extends State<Records> {
  bool _isLoading;
  SharedPreferences _sharedPreferences;

  List<int> _records = new List<int>();

  List<Widget> _resultados = new List();

  @override
  void initState() {
    _isLoading = true;

    _Loading().then((dynamic) {
      setState(() {
        _isLoading = false;
      });
    });
  }

  Future _Loading() async {
    _sharedPreferences = await SharedPreferences.getInstance();
    String recod = _sharedPreferences.getString("record");
    if (recod == null) {
      _records = [0, 0, 0, 0, 0];
    } else {
      List<int> aux = List<int>();
      _records = json.decode(recod).forEach((map) {
        aux.add(map as int);
      });
      _records = aux;
    }
    createResultados();
    await Future.delayed(Duration(seconds: 1));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Resultados"),
        centerTitle: true,
        actions: <Widget>[
          PopupMenuButton<Widget>(
            itemBuilder: (context) {
              return <PopupMenuEntry<Widget>>[
                PopupMenuItem<Widget>(
                  value: Text("Clear"),
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.storage),
                      Text("Vaciar resultados"),
                    ],
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  ),
                )
              ];
            },
            onSelected: (value) {
              Text text = value;
              if (text.data == "Clear") {
                setState(() {
                  _records = [0, 0, 0, 0, 0];
                });
                _sharedPreferences.setString("record", json.encode(_records));
                initState();
              }
            },
          ),
        ],
      ),
      body: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 50.0),
            child: ListView(
              itemExtent: MediaQuery.of(context).size.height / 9,
              padding: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width / 6),
              children: _resultados,
            ),
          ),
          Loading(
            isLoading: _isLoading,
          ),
        ],
      ),
    );
  }

  void createResultados() {
    List<Widget> widgets = new List();
    int position = 1;
    _records.forEach((number) {
      Card widget = Card(
        margin: EdgeInsets.only(bottom: 20.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            AutoSizeText(
              position.toString() + "º",
              maxLines: 1,
              style: TextStyle(
                fontSize: 40.0,
              ),
            ),
            AutoSizeText(
              number.toString() + " generados",
              maxLines: 1,
              style: TextStyle(
                fontSize: 20.0,
              ),
            ),
          ],
        ),
      );
      widgets.add(widget);
      position++;
    });

    setState(() {
      _resultados = widgets;
    });
  }
}
