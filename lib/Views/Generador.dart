import 'package:flutter/material.dart';
import 'package:gdp_v2/Utils/Storage.dart';
import 'package:gdp_v2/Views/Records.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:gdp_v2/Utils/FinalCountDialog.dart';
import 'package:gdp_v2/Models/Character.dart';
import 'package:gdp_v2/Utils/Loading.dart';
import 'dart:math';
import 'dart:convert';

class Generador extends StatefulWidget {
  @override
  GeneradorState createState() {
    return new GeneradorState();
  }
}

class GeneradorState extends State<Generador> {
  List<Widget> _container = List<Widget>();
  List<int> _records = List<int>(5);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => _requestPop(context),
      child: Scaffold(
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.check),
            elevation: 7.0,
            tooltip: "Resultados",
            onPressed:()=> _finalize(context)),
        appBar: AppBar(
          title: Text("Generador"),
          actions: <Widget>[
            PopupMenuButton<Widget>(
              itemBuilder: (context) {
                return <PopupMenuEntry<Widget>>[
                  PopupMenuItem<Widget>(
                    value: Text("Registro",),
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.storage,color: Theme.of(context).textTheme.body1.color),
                        Text("Resultados",style: TextStyle(color: Theme.of(context).textTheme.body1.color),),
                      ],
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    ),
                  )
                ];
              },
              onSelected: (value) {
                Text text = value;
                print(text.data);
                if (text.data == "Registro") {
                  if(_container.length == 0){
                    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context)=>Records()));
                  }else{
                    _finalize(context).whenComplete((){
                      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context)=>Records()));
                    });
                  }
                }
              },
            ),
          ],
        ),
        body: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Body(),
        ),
      ),
    );
  }

  Future<bool> _requestPop(BuildContext context) {
    if (_container.length > 0) {
      _finalize(context).then((dynamic) {
        Navigator.pop(context);
      });
      return Future.value(false);
    } else {
      return Future.value(true);
    }
  }

  Future _finalize(BuildContext context) async {
    int totalGenerados = _container.length;
    setState(() {
      _setRecord(totalGenerados);
      _container.clear();
    });
    _saveSharedPref();
    await showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) => FinalCountDialog(totalGenerados));
  }

  void _setRecord(int totalGenerados) {
    print(_records.length);
    int positions = _records.length - 1;
    bool finish = true;
    do {
      if (totalGenerados > _records[positions]) {
        if (positions == 0 || totalGenerados < _records[(positions - 1)]) {
          _insertPosition(positions, _records.length - 1, totalGenerados);
          finish = false;
        } else {
          positions--;
        }
      } else {
        finish = false;
      }
    } while (finish);
  }

  void _insertPosition(int position, int size, int value) {
    if (size == position) {
      _records[position] = value;
    } else {
      int aux = _records[position];
      _insertPosition(position + 1, size, aux);
      _records[position] = value;
    }
  }

  bool _isLoading;

  SharedPreferences _sharedPreferences;
  double _especialValue;
  Random _random;
  Character _type;

  static AudioCache player = new AudioCache();

  @override
  void initState() {
    _isLoading = true;
    _Loading().then((dynamic) {
      setState(() {
        _isLoading = false;
        player.loadAll([_type.sound, _type.soundEspecial]);
      });
    });
    _random = Random();
  }

  Future _Loading() async {
    _sharedPreferences = await SharedPreferences.getInstance();
    double value = _sharedPreferences.getDouble("especial_value");
    if (value == null) value = 0.0;
    String type = _sharedPreferences.getString("type");
    if (type == null) type = "pollo";
    String recod = _sharedPreferences.getString("record");
    if (recod == null) {
      _records = [0, 0, 0, 0, 0];
    } else {
      List<int> aux = List<int>();
      _records = json.decode(recod).forEach((map) {
        aux.add(map as int);
      });
      _records = aux;
    }
    setState(() {
      _especialValue = value;
      _type = Storage().getCharacter(type);
    });
  }

  void _saveSharedPref() {
    String records = json.encode(_records);
    _sharedPreferences.setString("record", records);
  }

  Widget Body() {
    return Container(
      child: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 60.0),
            child: SingleChildScrollView(
              reverse: true,
              child: Wrap(
                children: _container,
              ),
            ),
          ),
          Container(
            height: 50.0,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                color: Colors.transparent,
                border: Border.all(
                  color: Theme.of(context).accentColor,
                  width: 5.0,
                  style: BorderStyle.solid,
                )),
            child: Center(
              child: Text(
                  "Has generado " + _container.length.toString() + " pollos",
                style: TextStyle(
                  color: Theme.of(context).accentColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0,

                ),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              setState(() {
                print("tap");
                int ran = _random.nextInt(100);
                if (ran < _especialValue) {
                  player.play(_type.soundEspecial);
                  _container.add(_type.especial);
                } else {
                  player.play(_type.sound);
                  _container.add(_type.icon);
                }
              });
            },
          ),
          Loading(
            isLoading: _isLoading,
          ),
        ],
      ),
    );
  }
}
