import 'dart:async';

import 'package:flutter/material.dart';
import 'package:gdp_v2/Models/Character.dart';
import 'package:gdp_v2/Utils/Storage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:gdp_v2/Utils/Loading.dart';
import 'package:gdp_v2/Utils/ColorPicker.dart';
import 'package:gdp_v2/Models/Themes.dart';

class Configuracion extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Configuracion"),
          centerTitle: true,
        ),
        body: Conf());
  }
}

class Conf extends StatefulWidget {
  Conf();

  @override
  State<StatefulWidget> createState() => ConfState();
}

class ConfState extends State<Conf> {
  bool _bolEspecial = false;
  double _valueEspecial;
  SharedPreferences _sharedPreferences;

  List<Widget> _listCharacters;
  String _type = "Pollo";

  bool isLoad;

  @override
  void dispose() {
    super.dispose();
    _finishSharedPref();
  }

  @override
  void initState() {
    isLoad = true;
    _listCharacters = CharacterChoices().allCharacters.map((type) {
      return _createIconsButton(type);
    }).toList();
    _isLoading().then((dynamic) {
      setState(() {
        isLoad = false;
      });
    });
    super.initState();
  }

  Widget _createIconsButton(String type) {
    Character character = Storage().getCharacter(type);
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: Colors.transparent, width: 5.0),
      ),
      child: IconButton(
        icon: character.icon,
        onPressed: (() {
          setState(() {
            _type = character.id;
            _markCharacterChoice();
          });
        }),
        tooltip: character.id,
      ),
    );
  }

  void _markCharacterChoice() {
    List<Widget> aux = _listCharacters.map((character) {
      IconButton button = (character as Container).child;
      return (button.tooltip == _type)
          ? Container(
              decoration: BoxDecoration(
                border:
                    Border.all(color: Theme.of(context).hintColor, width: 5.0),
              ),
              child: button,
            )
          : Container(
              decoration: BoxDecoration(
                border: Border.all(color: Colors.transparent, width: 5.0),
              ),
              child: button,
            );
    }).toList();

    _listCharacters = aux;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        _buildBody(),
        Loading(
          isLoading: isLoad,
        ),
      ],
    );
  }

  void _finishSharedPref() async {
    bool checkEspecial =
        await _sharedPreferences.setDouble("especial_value", _valueEspecial);
    print("finishSP " + _type);
    bool checkType = await _sharedPreferences.setString("typeCharc", _type);
    if (!checkEspecial) {
      print("FALLO en ESPECIAL");
    }
    if (!checkType) {
      print("FALLO en Type");
    }
  }

  Future _isLoading() async {
    _sharedPreferences = await SharedPreferences.getInstance();
    double value = _sharedPreferences.getDouble("especial_value");
    if (value == null) value = 0.0;
    setState(() {
      _valueEspecial = value;
      _bolEspecial = value > 0;
    });
    String type = _sharedPreferences.getString("typeCharc");
    if (type == null) type = "Pollo";
    setState(() {
      _type = type;
      _markCharacterChoice();
    });
    await Future.delayed(Duration(seconds: 1));
  }

  void _onChangeCheckBox(bool isCheck) {
    setState(() {
      _bolEspecial = isCheck;
      if (!isCheck) {
        _valueEspecial = 0.0;
      }
    });
  }

  void _onChangeSlide(double value) {
    setState(() {
      _valueEspecial = value;
    });
  }

  Widget _buildBody() {
    return Container(
      padding: EdgeInsets.only(top: 50.0),
      child: ListView(
        padding: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width / 4),
        children: <Widget>[
          Card(
            elevation: 10.0,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Checkbox(
                        value: _bolEspecial,
                        onChanged: _onChangeCheckBox,
                      ),
                      Text(
                        "Especial",
                        style: TextStyle(
                          fontSize: 20.0,
                        ),
                      )
                    ],
                  ),
                  _bolEspecial
                      ? Container(
                          child: Column(
                            children: <Widget>[
                              Slider(
                                  min: 0.0,
                                  max: 100.0,
                                  value: _valueEspecial,
                                  divisions: 100,
                                  onChanged: _onChangeSlide),
                              Center(
                                child: Text(
                                  _valueEspecial.toStringAsFixed(0) + " %",
                                  style: TextStyle(
                                    fontSize: 20.0,
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                      : Container()
                ],
              ),
            ),
          ),
          Card(
            elevation: 10.0,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                children: <Widget>[
                  Text(
                    "Personaje",
                    style: TextStyle(
                      fontSize: 20.0,
                    ),
                  ),
                  Row(
                    children: _listCharacters,
                  )
                ],
              ),
            ),
          ),
          Card(
            elevation: 10.0,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                children: <Widget>[
                  Text(
                    "Tema",
                    style: TextStyle(
                      fontSize: 20.0,
                    ),
                  ),
                  ColorPicker(
                    onSelect: (theme) {
                      setState(() {
                        isLoad = true;
                        Storage.themeKey = theme;
                        _sharedPreferences.setString("theme", theme);
                        Storage.changeTheme(theme);
                        Timer(Duration(seconds: 1), () {
                          setState(() {
                            _markCharacterChoice();
                            isLoad = false;
                          });
                        });
                      });
                    },
                  ),
                ],
              ),
            ),
          ),
          Card(
            elevation: 10.0,
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: Row(
                children: <Widget>[
                  Checkbox(
                    value: Storage.darkTheme,
                    onChanged: (value) {
                      setState(() {
                        isLoad = true;
                        Storage.darkTheme = value;
                        _sharedPreferences.setBool("darkTheme", value);
                        Storage.changeTheme(Storage.themeKey);
                        Timer(Duration(seconds: 1), () {
                          setState(() {
                            _markCharacterChoice();
                            isLoad = false;
                          });
                        });
                      });
                    },
                  ),
                  Text(
                    "Modo\nOscuro",
                    maxLines: 2,
                    style: TextStyle(
                      fontSize: 20.0,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
