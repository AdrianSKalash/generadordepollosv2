import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:gdp_v2/Menu.dart';
import 'package:gdp_v2/Models/Themes.dart';
import 'package:gdp_v2/Utils/Loading.dart';

import 'package:gdp_v2/Utils/Storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() => runApp(MaterialApp(
      home: Intro(),
    ));

class Intro extends StatefulWidget {
  @override
  IntroState createState() => IntroState();
}

class IntroState extends State<Intro> with SingleTickerProviderStateMixin {
  bool isLoading;

  AnimationController _controller;
  Animation _animation;

  Color _value = Colors.white;

  String _message;

  SharedPreferences _sharedPreferences;


  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 250));

    _animation = ColorTween(begin: Colors.white, end: Colors.yellow).animate(_controller)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          _controller.reverse();
        }
        if (status == AnimationStatus.dismissed) {
          _controller.forward();
        }
      });
    _animation.addListener(() {
      setState(() {
        _value = _animation.value;
      });
    });
    _controller.forward();

    isLoading = true;
    _message = Storage().introMessages();
    _Loading().then((dynamic){
      new Timer(Duration(seconds: 10), _load);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          _buildBody(),
          Loading(
            isLoading: isLoading,
          ),
        ],
      ),
    );
  }

  Widget _buildBody() {
    return Container(
      color: Colors.blue,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Column(
              children: <Widget>[
                Text(
                  "Generador de Pollos",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 40.0,
                    fontWeight: FontWeight.bold,
                    foreground: Paint()..color = _value,
                  ),
                ),
                Text(
                  "Por un mundo con mas pollos",
                  style: TextStyle(
                    foreground: Paint()..color = Colors.white,
                  ),
                )
              ],
            ),
            Container(
              margin: EdgeInsets.all(30.0),
              padding: EdgeInsets.all(5.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                border: Border.all(width: 5.0),
              ),
              child: Text(
                _message,
                style: TextStyle(
                  foreground: Paint()..color = Colors.white,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void _load() {
    setState(() {
      isLoading = false;
    });
    Navigator.of(context).pushReplacement(new PageRouteBuilder(
        maintainState: true,
        opaque: true,
        pageBuilder: (context, _, __) => new Main(),
        transitionDuration: const Duration(milliseconds: 250),
        transitionsBuilder: (context, anim1, anim2, child) {
          return new FadeTransition(
            child: child,
            opacity: anim1,
          );
        }));
  }

  Future _Loading() async{
      _sharedPreferences = await SharedPreferences.getInstance();

      String theme = _sharedPreferences.getString("theme");
      bool darkTheme = _sharedPreferences.getBool("darkTheme");
      if(darkTheme == null) darkTheme = false;
      if(theme == null) theme = "blue";
      Storage.darkTheme = darkTheme;
      Storage.theme = ValueNotifier(ThemeChoices().getTheme(theme));
      Storage.themeKey = theme;
  }
}
