import 'package:flutter/material.dart';

class MenuButton extends StatelessWidget{

  final String text;
  final IconData icon;
  final VoidCallback onPress;

  const MenuButton(this.text,{Key key,this.icon, @required this.onPress}) : super(key: key);



  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 15.0),
            child: Icon(icon,color: Theme.of(context).textTheme.body1.color,),
          ),
          Text(text,style: TextStyle(color: Theme.of(context).textTheme.body1.color,
              fontSize: 15.0,fontWeight: FontWeight.bold),),
        ],
      ),
      onPressed: onPress,
    );
  }

}