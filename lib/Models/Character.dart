import 'package:flutter/material.dart';

class Character{

  final Image _icon;
  final Image _especial;
  final String _sound;
  final String _soundEspecial;
  final String _id;

  Character(this._icon, this._especial, this._sound, this._soundEspecial, this._id);

  String get sound => _sound;

  Image get especial => _especial;

  Image get icon => _icon;

  String get soundEspecial => _soundEspecial;

  String get id => _id;


}

class CharacterChoices {

  final List<String> allCharacters = <String>[
    "pollo",
    "pollodramatico"
  ];

  Character getCharacter(String type){
    String id;
    Image icon;
    Image especial;
    String sound;
    String soundEspecial;

    switch(type){
      case "pollodramatico":
        id = "Pollo Dramatico";
        icon = Image.asset('assets/pollodramatico_normal.png');
        especial =Image.asset('assets/pollodramatico_especial.png');
        sound = "pollodramatico_normal.mp3";
        soundEspecial = "pollodramatico_especial.mp3";
        break;
      case "pollo":
      default:
        id = "Pollo";
        icon = Image.asset('assets/pollo_normal.png');
        especial =Image.asset('assets/pollo_especial.png');
        sound = "sonido.mp3";
        soundEspecial = "sonido_rage.mp3";
        break;
    }

    return Character(icon, especial, sound, soundEspecial,id);
  }

}