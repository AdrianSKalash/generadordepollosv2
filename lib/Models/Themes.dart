import 'package:flutter/material.dart';
import 'package:gdp_v2/Utils/Storage.dart';

class ThemeColor extends StatelessWidget {
  final String _id;
  final Color _theme;
  final bool _isCheck;
  final GestureTapCallback onPress;

  ThemeColor(this._id, this._theme, this._isCheck, this.onPress);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Container(
        child: Material(
          elevation: 10.0,
          shape: const CircleBorder(),
          child: CircleAvatar(
            radius: 20.0,
            backgroundColor: _theme,
            child: _isCheck
                ? Icon(
                    Icons.check,
                    color: Theme.of(context).hintColor,
                    size: 40.0,
                  )
                : Container(),
          ),
        ),
      ),
    );
  }
}

class ThemeChoices {

  Map<String, Color> mapColor = {
    "blue": Color.fromARGB(255, 15, 161, 255),
    "red": Color.fromARGB(255, 253, 7, 13),
    "green" : Color.fromARGB(255, 137, 255, 0),
    "pink" : Color.fromARGB(255, 255, 0, 178),
    "orange" : Color.fromARGB(255, 255, 124, 0),
    "yellow" : Color.fromARGB(255, 255, 242, 0),
  };

  final ThemeData blue = ThemeData(
      textTheme: TextTheme(body1: TextStyle(color: Colors.white, fontFamily: 'Jura')),
      primaryColor: Color.fromARGB(255, 15, 161, 255),
      accentColor: Color.fromARGB(255, 36, 100, 255),
      buttonColor: Color.fromARGB(255, 36, 100, 255),
      cardColor: Color.fromARGB(255, 36, 100, 255),
      hintColor: Color.fromARGB(255, 255, 153, 0),
      unselectedWidgetColor: Color.fromARGB(255, 255, 153, 0),
      toggleableActiveColor: Color.fromARGB(255, 255, 153, 0),
      sliderTheme: SliderThemeData.fromPrimaryColors(
          primaryColor: Color.fromARGB(255, 255, 153, 0),
          primaryColorDark: Color.fromARGB(255, 255, 153, 0),
          primaryColorLight: Color.fromARGB(255, 255, 153, 0),
          valueIndicatorTextStyle:
          TextStyle(color: Color.fromARGB(255, 255, 153, 0))));

  final ThemeData blueDark = ThemeData(
      textTheme: TextTheme(body1: TextStyle(color: Colors.black, fontFamily: 'Jura')),
      scaffoldBackgroundColor: Colors.black12,
      primaryColor: Color.fromARGB(255, 0, 0, 152),
      accentColor: Color.fromARGB(255, 16, 156, 255),
      buttonColor: Color.fromARGB(255, 16, 156, 255),
      cardColor: Color.fromARGB(255, 16, 156, 255),
      hintColor: Color.fromARGB(255, 255, 198, 0),
      unselectedWidgetColor: Color.fromARGB(255, 255, 198, 0),
      toggleableActiveColor: Color.fromARGB(255, 255, 198, 0),
      sliderTheme: SliderThemeData.fromPrimaryColors(
          primaryColor: Color.fromARGB(255, 255, 198, 0),
          primaryColorDark: Color.fromARGB(255, 255, 198, 0),
          primaryColorLight: Color.fromARGB(255, 255, 198, 0),
          valueIndicatorTextStyle:
          TextStyle(color: Color.fromARGB(255, 255, 198, 0))));


  final ThemeData red = ThemeData(
      textTheme: TextTheme(body1: TextStyle(color: Colors.white, fontFamily: 'Jura')),
      primaryColor: Color.fromARGB(255, 253, 7, 13),
      accentColor: Color.fromARGB(255, 255, 117, 7),
      buttonColor: Color.fromARGB(255, 255, 117, 7),
      cardColor: Color.fromARGB(255, 255, 117, 7),
      hintColor: Color.fromARGB(255, 13, 255, 0),
      unselectedWidgetColor: Color.fromARGB(255, 13, 255, 0),
      toggleableActiveColor: Color.fromARGB(255, 13, 255, 0),
      sliderTheme: SliderThemeData.fromPrimaryColors(
          primaryColor: Color.fromARGB(255, 13, 255, 0),
          primaryColorDark: Color.fromARGB(255, 13, 255, 0),
          primaryColorLight: Color.fromARGB(255, 13, 255, 0),
          valueIndicatorTextStyle:
          TextStyle(color: Color.fromARGB(255, 13, 255, 0))));

  final ThemeData redDark = ThemeData(
      textTheme: TextTheme(body1: TextStyle(color: Colors.black, fontFamily: 'Jura', fontWeight: FontWeight.bold)),
      scaffoldBackgroundColor: Colors.black12,
      primaryColor: Color.fromARGB(255, 114, 0, 3),
      accentColor: Color.fromARGB(255, 255, 116, 112),
      buttonColor: Color.fromARGB(255, 255, 116, 112),
      cardColor: Color.fromARGB(255, 255, 116, 112),
      hintColor: Color.fromARGB(255, 3, 234, 255),
      unselectedWidgetColor: Color.fromARGB(255, 3, 234, 255),
      toggleableActiveColor: Color.fromARGB(255, 3, 234, 255),
      sliderTheme: SliderThemeData.fromPrimaryColors(
          primaryColor: Color.fromARGB(255, 3, 234, 255),
          primaryColorDark: Color.fromARGB(255, 3, 234, 255),
          primaryColorLight: Color.fromARGB(255, 3, 234, 255),
          valueIndicatorTextStyle:
          TextStyle(color: Color.fromARGB(255, 3, 234, 255))));


  final ThemeData green = ThemeData(
      textTheme: TextTheme(body1: TextStyle(color: Colors.white, fontFamily: 'Jura')),
      primaryColor: Color.fromARGB(255, 137, 255, 0),
      accentColor: Color.fromARGB(255, 0, 155, 67),
      dialogBackgroundColor: Color.fromARGB(255, 0, 155, 67),
      buttonColor: Color.fromARGB(255, 0, 155, 67),
      cardColor: Color.fromARGB(255, 0, 155, 67),
      hintColor: Color.fromARGB(255, 255, 0, 99),
      unselectedWidgetColor: Color.fromARGB(255, 255, 0, 99),
      toggleableActiveColor: Color.fromARGB(255, 255, 0, 99),
      sliderTheme: SliderThemeData.fromPrimaryColors(
          primaryColor: Color.fromARGB(255, 255, 0, 99),
          primaryColorDark: Color.fromARGB(255, 255, 0, 99),
          primaryColorLight: Color.fromARGB(255, 255, 0, 99),
          valueIndicatorTextStyle:
          TextStyle(color: Color.fromARGB(255, 255, 0, 99))));

  final ThemeData greenDark = ThemeData(
      textTheme: TextTheme(body1: TextStyle(color: Colors.black, fontFamily: 'Jura', fontWeight: FontWeight.bold)),
      scaffoldBackgroundColor: Colors.black12,
      primaryColor: Color.fromARGB(255, 6, 44, 0),
      accentColor: Color.fromARGB(255, 19, 139, 0),
      buttonColor: Color.fromARGB(255, 19, 139, 0),
      cardColor: Color.fromARGB(255, 19, 139, 0),
      hintColor: Color.fromARGB(255, 168, 73, 0),
      unselectedWidgetColor: Color.fromARGB(255, 168, 73, 0),
      toggleableActiveColor: Color.fromARGB(255, 168, 73, 0),
      sliderTheme: SliderThemeData.fromPrimaryColors(
          primaryColor: Color.fromARGB(255, 168, 73, 0),
          primaryColorDark: Color.fromARGB(255, 168, 73, 0),
          primaryColorLight: Color.fromARGB(255, 168, 73, 0),
          valueIndicatorTextStyle:
          TextStyle(color: Color.fromARGB(255, 168, 73, 0))));

  final ThemeData pink = ThemeData(
      textTheme: TextTheme(body1: TextStyle(color: Colors.white, fontFamily: 'Jura', fontWeight: FontWeight.bold)),
      primaryColor: Color.fromARGB(255, 255, 0, 178),
      accentColor: Color.fromARGB(255, 164, 0, 115),
      buttonColor: Color.fromARGB(255, 164, 0, 115),
      cardColor: Color.fromARGB(255, 164, 0, 115),
      hintColor: Color.fromARGB(255, 187, 255, 0),
      unselectedWidgetColor: Color.fromARGB(255, 187, 255, 0),
      toggleableActiveColor: Color.fromARGB(255, 187, 255, 0),
      sliderTheme: SliderThemeData.fromPrimaryColors(
          primaryColor: Color.fromARGB(255, 187, 255, 0),
          primaryColorDark: Color.fromARGB(255, 187, 255, 0),
          primaryColorLight: Color.fromARGB(255, 187, 255, 0),
          valueIndicatorTextStyle:
          TextStyle(color: Color.fromARGB(255, 187, 255, 0))));

  final ThemeData pinkDark = ThemeData(
      textTheme: TextTheme(body1: TextStyle(color: Colors.black, fontFamily: 'Jura', fontWeight: FontWeight.bold)),
      scaffoldBackgroundColor: Colors.black12,
      primaryColor: Color.fromARGB(255, 43, 0, 24),
      accentColor: Color.fromARGB(255, 135, 0, 76),
      buttonColor: Color.fromARGB(255, 135, 0, 76),
      cardColor: Color.fromARGB(255, 135, 0, 76),
      hintColor: Color.fromARGB(255, 0, 132, 9),
      unselectedWidgetColor: Color.fromARGB(255, 0, 132, 9),
      toggleableActiveColor: Color.fromARGB(255, 0, 132, 9),
      sliderTheme: SliderThemeData.fromPrimaryColors(
          primaryColor: Color.fromARGB(255, 0, 132, 9),
          primaryColorDark: Color.fromARGB(255, 0, 132, 9),
          primaryColorLight: Color.fromARGB(255, 0, 132, 9),
          valueIndicatorTextStyle:
          TextStyle(color: Color.fromARGB(255, 0, 132, 9))));

  final ThemeData orange = ThemeData(
      textTheme: TextTheme(body1: TextStyle(color: Colors.white, fontFamily: 'Jura')),
      primaryColor: Color.fromARGB(255, 255, 124, 0),
      accentColor: Color.fromARGB(255, 217, 150, 0),
      buttonColor: Color.fromARGB(255, 217, 150, 0),
      cardColor: Color.fromARGB(255, 217, 150, 0),
      hintColor: Color.fromARGB(255, 6, 216, 255),
      unselectedWidgetColor: Color.fromARGB(255, 6, 216, 255),
      toggleableActiveColor: Color.fromARGB(255, 6, 216, 255),
      sliderTheme: SliderThemeData.fromPrimaryColors(
          primaryColor: Color.fromARGB(255, 6, 216, 255),
          primaryColorDark: Color.fromARGB(255, 6, 216, 255),
          primaryColorLight: Color.fromARGB(255, 6, 216, 255),
          valueIndicatorTextStyle:
          TextStyle(color: Color.fromARGB(255, 6, 216, 255))));

  final ThemeData orangeDark = ThemeData(
      textTheme: TextTheme(body1: TextStyle(color: Colors.black, fontFamily: 'Jura', fontWeight: FontWeight.bold)),
      scaffoldBackgroundColor: Colors.black12,
      primaryColor: Color.fromARGB(255, 54, 26, 0),
      accentColor: Color.fromARGB(255, 115, 55, 0),
      buttonColor: Color.fromARGB(255, 115, 55, 0),
      cardColor: Color.fromARGB(255, 115, 55, 0),
      hintColor: Color.fromARGB(255, 10, 34, 114),
      unselectedWidgetColor: Color.fromARGB(255, 10, 34, 114),
      toggleableActiveColor: Color.fromARGB(255, 10, 34, 114),
      sliderTheme: SliderThemeData.fromPrimaryColors(
          primaryColor: Color.fromARGB(255, 10, 34, 114),
          primaryColorDark: Color.fromARGB(255, 10, 34, 114),
          primaryColorLight: Color.fromARGB(255, 10, 34, 114),
          valueIndicatorTextStyle:
          TextStyle(color: Color.fromARGB(255, 10, 34, 114))));

  final ThemeData yellow = ThemeData(
      textTheme: TextTheme(body1: TextStyle(color: Colors.white, fontFamily: 'Jura')),
      primaryColor: Color.fromARGB(255, 255, 242, 0),
      accentColor: Color.fromARGB(255, 217, 206, 0),
      buttonColor: Color.fromARGB(255, 217, 206, 0),
      cardColor: Color.fromARGB(255, 217, 206, 0),
      hintColor: Color.fromARGB(255, 142, 18, 255),
      unselectedWidgetColor: Color.fromARGB(255, 142, 18, 255),
      toggleableActiveColor: Color.fromARGB(255, 142, 18, 255),
      sliderTheme: SliderThemeData.fromPrimaryColors(
          primaryColor: Color.fromARGB(255, 142, 18, 255),
          primaryColorDark: Color.fromARGB(255, 142, 18, 255),
          primaryColorLight: Color.fromARGB(255, 142, 18, 255),
          valueIndicatorTextStyle:
          TextStyle(color: Color.fromARGB(255, 142, 18, 255))));


  final ThemeData yellowDark = ThemeData(
      textTheme: TextTheme(body1: TextStyle(color: Colors.black, fontFamily: 'Jura', fontWeight: FontWeight.bold)),
      scaffoldBackgroundColor: Colors.black12,
      primaryColor: Color.fromARGB(255, 54, 50, 0),
      accentColor: Color.fromARGB(255, 115, 106, 0),
      buttonColor: Color.fromARGB(255, 115, 106, 0),
      cardColor: Color.fromARGB(255, 115, 106, 0),
      hintColor: Color.fromARGB(255, 119, 0, 100),
      unselectedWidgetColor: Color.fromARGB(255, 119, 0, 100),
      toggleableActiveColor: Color.fromARGB(255, 119, 0, 100),
      sliderTheme: SliderThemeData.fromPrimaryColors(
          primaryColor: Color.fromARGB(255, 119, 0, 100),
          primaryColorDark: Color.fromARGB(255, 119, 0, 100),
          primaryColorLight: Color.fromARGB(255, 119, 0, 100),
          valueIndicatorTextStyle:
          TextStyle(color: Color.fromARGB(255, 119, 0, 100))));


  ThemeData getTheme(String id) {
    ThemeData theme;
    switch (id) {
      case "red":
        theme = Storage.darkTheme?redDark:red;
        break;
      case "green":
        theme = Storage.darkTheme?greenDark:green;
        break;
      case "pink":
        theme = Storage.darkTheme?pinkDark:pink;
        break;
      case "orange":
        theme = Storage.darkTheme?orangeDark:orange;
        break;
      case "yellow":
        theme = Storage.darkTheme?yellowDark:yellow;
        break;
      case "blue":
      default:
        theme = Storage.darkTheme?blueDark:blue;
        break;
    }

    return theme;
  }
}
