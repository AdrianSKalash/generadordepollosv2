import 'package:flutter/material.dart';
import 'package:gdp_v2/Utils/Storage.dart';
import 'package:gdp_v2/Views/Records.dart';

class FinalCountDialog extends StatelessWidget{

  final int value;


  const FinalCountDialog(this.value);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Center(child: Text("Resultados"),),
      content: Text(message()),
      actions: <Widget>[
        FlatButton(
          child: Text("Aceptar"),
          onPressed: (){
            Navigator.of(context).pop();
          },
        ),
      ],
    );
  }

  String message(){
    String message = Storage().resultMessages(value);
    return "Has generado "+value.toString()+" pollos \n"+message;
  }



}