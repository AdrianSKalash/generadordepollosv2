
import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:gdp_v2/Models/Character.dart';
import 'package:gdp_v2/Models/Themes.dart';

class Storage {

  final List<String> _introMsg = [
    "Un momento mientras traemos las aves, que no sabes como pican",
    "El dios de los pollos quiere que esperes un poco. No nos atrevemos a contradecirle, asi que espera un momento",
    "Un momento mientras la gallina cruza la carretera, el porque no lo sabemos",
    "Ahora disponible en gallino",
    "Las aves han montado una huelga por falta de alpiste, espera un momento mientras lo compramos",
    "Estamos maquillando a las aves, que tambien tienen el derecho de estar monas",
    "Debido a la escasez momentanea de aves que tenemos, la aplicacion podria generar vacas amarillas como sustitutos",
    "Nuestros pollos estan alimentados con el mejor pienso que podemos robarle al vecino",
    "No se recomienda el uso de la aplicacion a menores de 3 años, los pollos pueden ponerse agresivos",
    "El maximo numero de aves que puede generar esta apliacion excede el limite de lo absurdo. Utilizar con precaucion",
    "Esta aplicacion cumple con las normativas aprobadas por el pollo mas listo que encontramos por ahi",
    "Los pollos puede contener trazas de frutos secos",
    "Gracias por ayudar a crear un mundo dominado por los pollos. El dios pollo sera benevolo contigo",
  ];

  String introMessages(){
    return _introMsg[Random().nextInt(_introMsg.length - 1)];
  }

  final List<String> _resulMsg = const <String>[
    "Esto es un generador. ¿Lo entiendes? GENERADOR. Asi que empieza a generar antes de que los pollos se enfaden.",
    "¿En serio?¿1?¿Crees que esto es una broma? Por mi te suspendia en la vida",
    "Que gracioso. Has generado 2 porque 1 no era suficiente. Nobel del humor. Y lo sigiente que, ¿3?",
    "Estas desperdiciando el potencial del generador. Replanteate tu vida mientras miras por una ventana con musica triste como en un videoclip",
    "Bueno algo es algo. La invasion aviar no se hizo en un dia.",
    "Veo que ya vas cogiendo el gusto por generar ¿no?",
    "No esta mal, nada mal. Incluso estas empezando a caerme bien",
    "Hay muchos pollos felices, el mundo ahora es un lugar un poquito mejor :)",
    "Guau, la verdad es que no esperaba tanto de ti. A partir de ahora ya no me ire riendo de ti.",
    "Pollos a tu derecha, pollos a tu izquierda, pollos por todos los lados. Ahora nadie podra escapar a su furia",
    "Hay sobrepoblacion de pollos. Los pollos han agotado los recursos del planeta, y ahora siendo superiores, se van al espacio a colonizar el resto del universo. Has condenado a la raza humana a la extincion. Enhorabuena.",
    "3RR0R en M4trix. Has alcanzado un nuevo un nuevo limite en la escala de lo absurdo. Seras adorado como un dios. EL DIOS POLLO"
  ];

  String resultMessages(int value){
    String message;
    if(value == 0){
      message = _resulMsg[0];
    }else if(value == 1){
      message = _resulMsg[1];
    }else if(value == 2){
      message = _resulMsg[2];
    }else if(value < 10){
      message = _resulMsg[3];
    }else if(value < 50){
      message = _resulMsg[4];
    }else if(value < 100){
      message = _resulMsg[5];
    }else if(value < 200){
      message = _resulMsg[6];
    }else if(value < 300){
      message = _resulMsg[7];
    }else if(value < 400){
      message = _resulMsg[8];
    }else if(value < 500){
      message = _resulMsg[9];
    }else if(value < 1000){
      message = _resulMsg[10];
    }else{
      message = _resulMsg[11];
    }
    return message;
  }

  static Map<String,Character> _characterCache = Map<String,Character>();

  Character getCharacter(String type){
    if(_characterCache.containsKey(type)){
      return _characterCache[type];
    }else{
      Character character = CharacterChoices().getCharacter(type);
      _characterCache[type]= character;
      return character;
    }
  }

  static String _themeKey;
  static String get themeKey => _themeKey;
  static set themeKey(String value) {
    _themeKey = value;
  }

  static ValueNotifier<ThemeData> _theme;
  static ValueNotifier<ThemeData> get theme => _theme;
  static set theme(ValueNotifier<ThemeData> value) {
    _theme = value;
  }
  static void changeTheme(String theme){
      _theme.value = ThemeChoices().getTheme(theme);
  }

  static bool _darkTheme;
  static bool get darkTheme => _darkTheme;
  static set darkTheme(bool value) {
    _darkTheme = value;
  }


}

class ValueTheme {

}