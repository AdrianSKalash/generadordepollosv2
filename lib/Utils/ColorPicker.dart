import 'package:flutter/material.dart';
import 'package:gdp_v2/Models/Themes.dart';
import 'package:gdp_v2/Utils/Storage.dart';

class ColorPicker extends StatefulWidget {
  final ValueChanged<String> onSelect;

  const ColorPicker({Key key, this.onSelect})
      : super(key: key);

  @override
  ColorPickerState createState() {
    return new ColorPickerState();
  }
}

class ColorPickerState extends State<ColorPicker> {
  List<ThemeColor> _themes = List<ThemeColor>();

  String _colorSelected;

  @override
  void initState() {
    super.initState();
    _colorSelected = Storage.themeKey;
    _refreshList();
  }

  void _refreshList() {
    _themes.clear();
    setState(() {
      Map<String, Color> themes = ThemeChoices().mapColor;
      for (String color in themes.keys) {
        bool isSelected = _colorSelected == color;
        _themes.add(ThemeColor(color, themes[color], isSelected, () {
          setState(() {
            _colorSelected = color;
            widget.onSelect(color);
          });
          _refreshList();
        }));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Wrap(
        direction: Axis.horizontal,
        runSpacing: 15.0,
        spacing: 15.0,
        children: _themes,
      ),
    );
  }
}
