import 'package:flutter/material.dart';

class Loading extends StatefulWidget{
  final bool isLoading;

  const Loading({@required this.isLoading});

  @override
  LoadingState createState() => LoadingState();

}

class LoadingState extends State<Loading> with SingleTickerProviderStateMixin{



  AnimationController _controller;

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _controller = AnimationController(
      duration: Duration(milliseconds: 1500),
      vsync: this,
    );

    _controller.repeat();
  }

  @override
  Widget build(BuildContext context) {
    return widget.isLoading? Container(
      color: Colors.black.withOpacity(0.20),
      child: Center(
        child: AnimatedBuilder(
          animation: _controller,
          child: Image.asset("assets/pollo_normal.png"),
          builder: (BuildContext context, Widget _widget) {
            return new Transform.rotate(
              angle: _controller.value * -6.0,
              child: _widget,
            );
          },) ,
      ),
    ): Container();
  }
}
