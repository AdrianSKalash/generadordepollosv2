import 'package:flutter/material.dart';
import 'package:gdp_v2/Models/MenuButton.dart';
import 'package:gdp_v2/Utils/Storage.dart';
import 'package:gdp_v2/Views/Generador.dart';
import 'package:gdp_v2/Views/Configuracion.dart';
import 'package:gdp_v2/Views/Records.dart';
import 'dart:io';

class Main extends StatefulWidget{


  @override
  MainState createState() {
    return new MainState();
  }
}

class MainState extends State<Main> {

  ValueNotifier<ThemeData> notifierTheme;
  ThemeData _theme;

  @override
  void initState() {
     super.initState();
     notifierTheme = Storage.theme;
     _theme = notifierTheme.value;
     print(_theme);
     notifierTheme.addListener((){
       setState(() {
         print("cambio");
         _theme = notifierTheme.value;
       });
     });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Generador de Pollos',
      theme: _theme,
      home: Menu(),
    );
  }

}


class Menu extends StatefulWidget {

  Menu();

  @override
  MenuState createState() {
    return new MenuState();
  }
}

class MenuState extends State<Menu> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Generador de Pollos"),
        centerTitle: true,
      ),
      body: Center(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width/4),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              MenuButton(
                "Generador",
                icon: Icons.add_to_queue,
                onPress:(){
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => Generador()));
                },
              ),
              MenuButton(
                "Configuracion",
                icon: Icons.filter_vintage,
                onPress:(){
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => Configuracion()));
                },
              ),
              MenuButton(
                "Resultados",
                icon: Icons.storage,
                onPress: (){
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => Records()));
                },
              ),
              MenuButton(
                "Salir",
                icon: Icons.exit_to_app,
                onPress: () {
                  exit(0);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

}


